#include <vtkActor.h>
#include <vtkBoxRepresentation.h>
#include <vtkBoxWidget2.h>
#include <vtkCommand.h>
#include <vtkInteractorStyleJoystickActor.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include <vtkInteractorStyleTrackballActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkInteractorStyle3DCursor.h>
#include <vtkInteractorStyleSwitch.h>
#include <vtkNamedColors.h>
#include <vtkNew.h>
#include <vtkPLYReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSphereSource.h>
#include <vtkTransform.h>

namespace
{
//------------------------------------------------------------------------------
class vtkBoxCallback : public vtkCommand
{
public:
  static vtkBoxCallback* New() { return new vtkBoxCallback; }

  vtkBoxCallback() = default;

  void SetActor(vtkActor* actor)
  {
    m_actor = actor;
  }

  virtual void Execute(vtkObject* caller, unsigned long, void*)
  {
    vtkSmartPointer<vtkBoxWidget2> boxWidget =
        dynamic_cast<vtkBoxWidget2*>(caller);

    vtkNew<vtkTransform> t;

    dynamic_cast<vtkBoxRepresentation*>(boxWidget->GetRepresentation())
        ->GetTransform(t);
    this->m_actor->SetUserTransform(t);
  }

  vtkActor* m_actor;
};

//------------------------------------------------------------------------------
class vtkCursorCallback : public vtkCommand
{
public:
  static vtkCursorCallback* New() { return new vtkCursorCallback; }

  vtkCursorCallback() = default;

  void SetStyle(vtkInteractorStyle3DCursor* style)
  {
    m_style = style;
  }

  void SetInteractor(vtkRenderWindowInteractor* interactor)
  {
    m_interactor = interactor;
  }

  virtual void Execute(vtkObject*, unsigned long, void*)
  {
    switch (m_interactor->GetKeyCode())
    {
      case 's':
      case 'S':
        m_style->SetCursorType(vtkInteractorStyle3DCursor::SPHERE);
        m_interactor->Render();
        break;
      case 'c':
      case 'C':
        m_style->SetCursorType(vtkInteractorStyle3DCursor::CROSS);
        m_interactor->Render();
        break;
      default:
        break;
    }
  }

  vtkInteractorStyle3DCursor* m_style;
  vtkRenderWindowInteractor* m_interactor;
};

//------------------------------------------------------------------------------
class vtkStyleCallback : public vtkCommand
{
public:
  static vtkStyleCallback* New() { return new vtkStyleCallback; }

  vtkStyleCallback() = default;

  void SetStyle(vtkInteractorStyle3DCursor* style)
  {
    m_style = style;
  }

  void SetInteractor(vtkRenderWindowInteractor* interactor)
  {
    m_interactor = interactor;
  }

  virtual void Execute(vtkObject*, unsigned long, void*)
  {
    switch (m_interactor->GetKeyCode())
    {
      case 'j':
      case 'J':
        m_style->SetCurrentStyle(2);
        break;
      case 'k':
      case 'K':
        m_style->SetCurrentStyle(3);
        break;
      case 't':
      case 'T':
        m_style->SetCurrentStyle(0);
        break;
      case 'y':
      case 'Y':
        m_style->SetCurrentStyle(1);
        break;
      default:
        break;
    }
  }

  vtkInteractorStyle3DCursor* m_style;
  vtkRenderWindowInteractor* m_interactor;
};
} // namespace

//------------------------------------------------------------------------------
int main(int vtkNotUsed(argc), char* vtkNotUsed(argv)[])
{
  vtkNew<vtkNamedColors> colors;

  vtkNew<vtkPLYReader> reader;
  reader->SetFileName("/home/tgalland/Data/dragon.ply");

  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(reader->GetOutputPort());

  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper);

  vtkNew<vtkRenderer> renderer;
  renderer->AddActor(actor);
  renderer->SetBackground(colors->GetColor3d("Blue").GetData());
  renderer->ResetCamera();

  vtkNew<vtkRenderWindow> renderWindow;
  renderWindow->AddRenderer(renderer);
  renderWindow->SetWindowName("Cursor");
  renderWindow->SetSize(800, 600);

  vtkNew<vtkInteractorStyle3DCursor> interactorStyle;
  vtkNew<vtkInteractorStyleTrackballCamera> trackBallCamera;
  vtkNew<vtkInteractorStyleTrackballActor> trackBallActor;
  vtkNew<vtkInteractorStyleJoystickCamera> joystickCamera;
  vtkNew<vtkInteractorStyleJoystickActor> joystickActor;
  interactorStyle->AddStyle(trackBallCamera);
  interactorStyle->AddStyle(trackBallActor);
  interactorStyle->AddStyle(joystickCamera);
  interactorStyle->AddStyle(joystickActor);
  interactorStyle->SetCursorSize(20);

  vtkNew<vtkRenderWindowInteractor> renderWindowInteractor;
  renderWindowInteractor->SetRenderWindow(renderWindow);
  renderWindowInteractor->SetInteractorStyle(interactorStyle);

  // Add box widget
  vtkNew<vtkBoxWidget2> boxWidget;
  boxWidget->SetInteractor(renderWindowInteractor);
  boxWidget->GetRepresentation()->SetPlaceFactor(1); // Default is 0.5
  boxWidget->GetRepresentation()->PlaceWidget(actor->GetBounds());

  // Set up a callback for the interactor to call so we can manipulate the actor
  vtkNew<vtkBoxCallback> boxCallback;
  boxCallback->SetActor(actor);
  boxWidget->AddObserver(vtkCommand::InteractionEvent, boxCallback);
  // boxWidget->On();

  // Set up a callback to change the cursor type
  vtkNew<vtkCursorCallback> cursorCallback;
  cursorCallback->SetStyle(interactorStyle);
  cursorCallback->SetInteractor(renderWindowInteractor);
  renderWindowInteractor->AddObserver(vtkCommand::KeyPressEvent, cursorCallback);

  // Set up a callback to change the internal style
  vtkNew<vtkStyleCallback> styleCallback;
  styleCallback->SetStyle(interactorStyle);
  styleCallback->SetInteractor(renderWindowInteractor);
  renderWindowInteractor->AddObserver(vtkCommand::KeyPressEvent, styleCallback);

  renderWindow->HideCursor();
  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
